<?php

namespace Greetik\BEInterfaceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BEInterfaceBundle:_Default:index.html.twig');
    }
}
